import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import pagination from 'laravel-vue-pagination'

import VueTheMask from 'vue-the-mask'
// import money from 'v-money'

import 'jQuery'
import 'admin-lte'
import '@fortawesome/fontawesome-free/js/all.min.js'

Vue.config.productionTip = false

Vue.use(VueTheMask)
// Vue.use(money, {prefix: 'R$', precision: 2})

Vue.filter('formatMoney', (item: number | string) => {
  if (!item) return
  let numberFloat = item
  if (typeof item == 'string') numberFloat = parseFloat(item)
  return ( numberFloat as number ).toFixed(2)
})

Vue.component('pagination', pagination);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
