import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/Home.vue'),
    meta: {
      requiresAuth: true,
      details: {
        title: 'Início',
        breadcrumb: [
          {
            name: 'Início',
            last: true
          }
        ]
      }
    }
  },
  {
    path: '/os',
    name: 'os',
    component: () => import('@/views/OS.vue'),
    meta: {
      requiresAuth: true,
      details: {
        title: 'Ordens de Serviço',
        breadcrumb: [
          {
            name: 'Início',
            href: '/'
          },
          {
            name: 'Ordens de Serviço',
            last: true
          }
        ]
      }
    }
  },

  {
    path: '/os/data/:id?',
    name: 'os-data',
    component: () => import('@/views/OSData.vue'),
    meta: {
      requiresAuth: true,
      details: {
        title: 'Ordem de Serviço',
        breadcrumb: [
          {
            name: 'Início',
            href: '/'
          },
          {
            name: 'Ordens de Serviço',
            href: '/os'
          },
          {
            name: 'OS',
            last: true
          }
        ]
      }
    }
  },

  {
    path: '/os/print/:id',
    name: 'os-print',
    component: () => import('@/views/Report.vue'),
    meta: {
      requiresAuth: true,
      details: {
        title: 'Impressão',
        breadcrumb: [
          {
            name: 'Início',
            href: '/'
          },
          {
            name: 'Ordens de Serviço',
            href: '/os'
          },
          {
            name: 'OS',
            href: '/os'
          },
          {
            name: 'Impressão',
            last: true
          }
        ]
      }
    }
  },

  {
    path: '/status',
    name: 'status',
    component: () => import('@/views/Status.vue'),
    meta: {
      requiresAuth: true,
      details: {
        title: 'Status',
        breadcrumb: [
          {
            name: 'Início',
            href: '/'
          },
          {
            name: 'Status',
            last: true
          }
        ]
      }
    }
  },

  {
    path: '/consult',
    name: 'consult',
    component: () => import('@/views/OS.vue'),
    meta: {
      requiresAuth: true,
      details: {
        title: 'Consultas',
        breadcrumb: [
          {
            name: 'Início',
            href: '/'
          },
          {
            name: 'Consultas',
            last: true
          }
        ]
      }
    }
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/Auth/Login.vue')
  },
  {
    path: '/logout',
    name: 'logout',
    component: () => import('@/views/OS.vue'),
    meta: { requiresAuth: true }
  },
  {
    path: '*',
    name: 'error404',
    component: () => import('@/views/errors/404.vue'),
    meta: {
      details: {
        title: '404 - Não encontrado',
        breadcrumb: [
          {
            name: 'Início',
            href: '/',
          },
          {
            name: 'Erro',
            last: true
          },
        ]
      }
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
