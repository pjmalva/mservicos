import {
  VuexModule, Module, getModule, Mutation, Action
} from 'vuex-module-decorators'

import {
  ServiceOrder, ServiceOrderItem, Product, Service, Business, User,
  ServiceOrderDetails, ServiceOrderEquipment, ServiceOrderStatus, Parameters
} from '@/types/modules'//'../../types/modules'

import store from '@/store'

@Module({
  store,
  name: 'serviceOrder',
  namespaced: true,
  dynamic: true,
  preserveState: localStorage.getItem('mg-asd123124ewdfaw43') !== null,
})
class ServiceOrderModule extends VuexModule{
  private order: ServiceOrder | null = null
  private details: ServiceOrderDetails | null = null
  private client: Business | null = null
  private equipment: ServiceOrderEquipment | null = null
  private items: Array<ServiceOrderItem> = []
  private statuses: Array<ServiceOrderStatus> = []
  private currentStatus: ServiceOrderStatus | null = null
  private user: User | null = null
  private parameters: Parameters | null = null

  public get serviceOrderParameters() {
    return this.parameters
  }

  public get serviceOrder() {
    return this.order
  }

  public get serviceDetails() {
    return this.details
  }

  public get serviceEquipment() {
    return this.equipment
  }

  public get serviceClient() {
    return this.client
  }

  public get serviceOrderItems(): Array<ServiceOrderItem> {
    return this.items
  }

  public get serviceOrderStatus() {
    return this.currentStatus
  }

  public get serviceOrderStatuses() {
    return this.statuses
  }

  public get serviceUser() {
    return this.user
  }

  @Mutation emptyServiceOrder() {
    this.order = null
    this.details = null
    this.client = null
    this.equipment = null
    this.items = []
    this.statuses = []
    this.currentStatus = null
    this.user = null
  }

  @Mutation setServiceOrderParameters(parameters: Parameters) { this.parameters = parameters }
  @Mutation setServiceOrder(order: ServiceOrder) { this.order = order }
  @Mutation setServiceOrderClient(client: Business) { this.client = client }
  @Mutation setServiceOrderDetails(details: ServiceOrderDetails) { this.details = details }
  @Mutation setServiceOrderEquipment(equipment: ServiceOrderEquipment) { this.equipment = equipment }
  @Mutation setServiceOrderCurrentStatus(currentStatus: ServiceOrderStatus) { this.currentStatus = currentStatus }

  @Mutation setServiceOrderStatuses(status: ServiceOrderStatus) {
    this.statuses = [ ...this.statuses, status ]
  }

  @Mutation setServiceOrderItem(item: ServiceOrderItem) {
    const itemIndex = this.items.findIndex(i => {
      return (i.item as Product | Service).id == (item.item as Product | Service).id
    })

    if (itemIndex >= 0) {
      this.items[itemIndex].quantity += 1
    } else {
      this.items = [...this.items, item]
    }

    this.order.value = (this.order.value || 0) + ((item.quantity * item.value) - (item.discount || 0))
  }

  @Mutation cleanItems () { this.items = [] }

  @Mutation deleteItem (item: ServiceOrderItem) {
    const items = this.items.filter(elem => {
      return (elem.item as Product | Service).id != (item.item as Product | Service).id
    })
    this.items = [...items]
  }


  @Mutation loadOSFromData (data: any) {
    const user: any = {};
    if (data.user) {
      user.id = data.user.id
      user.name = data.user.name
      user.email = data.user.email
      user.password = ''
      user.photo = data.user.photo
    }
    this.user = ( user as User )

    const client: any = {};
    if (data.business) {
      client.id = data.business.id
      client.cnpj = data.business.cnpj
      client.ie = data.business.ie
      client.im = data.business.im
      client.name = data.business.name
      client.fantasyName = data.business.fantasy_name
      client.logo = data.business.logo
      client.cep = data.business.cep
      client.address = data.business.address
      client.complement = data.business.complement
      client.number = data.business.number
      client.neighborhood = data.business.neighborhood
      client.city = data.business.city
      client.state = data.business.state
      client.country = data.business.country
      client.phone = data.business.phone
      client.cellphone = data.business.cellphone
      client.email = data.business.email
    }
    this.client = ( client as Business )

    const order: any = {};
    order.id = data.id
    order.number = data.number
    order.userId = data.user_id
    order.statusId = data.status_id
    order.value = data.value
    order.createdAt = data.created_at
    order.updatedAt = data.updated_at
    // order.business = this.client
    // order.user = this.user
    // order.status = this.status
    this.order = ( order as ServiceOrder )

    const details: any = {};
    details.companyId = data.company_id
    details.resultCenterId = data.result_center_id
    details.stockTypeId = data.stock_type_id
    details.priceTableId = data.price_table_id
    details.limitWarranty = data.limit_warranty
    details.haveWarranty = data.have_warranty
    details.wasApproved = data.was_approved
    details.wasCanceled = data.was_canceled
    details.motive = data.motive
    this.details = ( details as ServiceOrderDetails )

    const equipment: any = {};
    equipment.technicalReport = data.technical_report
    equipment.description = data.description
    equipment.accessories = data.accessories
    equipment.observations = data.observations
    if (data.equipment) {
      equipment.id = data.equipment_id
      equipment.serieNumber = data.equipment.serie_number
      equipment.name = data.equipment.name
      equipment.image = data.equipment.image
      equipment.defects = data.equipment.defects
    }
    this.equipment = ( equipment as ServiceOrderEquipment )

    data.status.forEach(( element: any ) => {
      const status: any = {};
      status.id = element.id
      status.serviceOrderId = element.service_order_id
      status.statusId = element.status_id
      status.userId = element.user_id
      status.name = element.name
      status.color = element.color
      status.description = element.description
      status.start = element.start
      status.finish = element.finish
      this.statuses = [ ...this.statuses, ( status as ServiceOrderStatus ) ]
    });


    const currentStatus: any = {};
    currentStatus.id = data.id
    currentStatus.serviceOrderId = data.current_status.service_order_id
    currentStatus.statusId = data.current_status.status_id
    currentStatus.userId = data.current_status.user_id
    currentStatus.name = data.current_status.name
    currentStatus.color = data.current_status.color
    currentStatus.description = data.current_status.description
    currentStatus.start = data.current_status.start
    currentStatus.finish = data.current_status.finish
    this.currentStatus = ( currentStatus as ServiceOrderStatus)

    data.items.forEach(( element: any ) => {
      const item: any = {};
      item.id = element.id
      item.serviceOrderId = element.service_order_id
      item.userId = element.user_id
      item.itemId = element.item_id
      item.type = element.type
      item.quantity = element.quantity
      item.value = element.value
      item.discount = element.discount
      item.description = element.description
      item.wasAproved = element.was_aproved
      // item.serviceOrder = element.service_order
      item.user = element.user
      item.item = element.item
      this.items = [ ...this.items, ( item as ServiceOrderItem ) ]
    });
  }

  @Action({commit: 'setServiceOrderParameters'})
  public setParameters(parameters: Parameters) {
    return parameters
  }

  @Action({commit: 'setServiceOrder'})
  public createServiceOrder(order: ServiceOrder) {
    return order
  }

  @Action({commit: 'setServiceOrderItem' })
  public addOsItem(item: ServiceOrderItem): ServiceOrderItem {
    return item
  }

  @Action({commit: 'setServiceOrderClient' })
  public setClient(client: Business): Business {
    return client
  }

  @Action({commit: 'setServiceOrderEquipment' })
  public setEquipment(equipment: ServiceOrderEquipment): ServiceOrderEquipment {
    return equipment
  }

  @Action({commit: 'setServiceOrderCurrentStatus' })
  public setStatus(status: ServiceOrderStatus): ServiceOrderStatus {
    return status
  }

  @Action({commit: 'setServiceOrderDetails' })
  public setDetails(details: ServiceOrderDetails): ServiceOrderDetails {
    return details
  }

  @Action({commit: 'emptyServiceOrder' })
  public cleanServiceOrder() {
    return null
  }

  @Action({commit: 'cleanItems' })
  public cleanServiceOrderItems() {
    return []
  }

  @Action({commit: 'deleteItem' })
  public deleteServiceOrderItem(item: ServiceOrderItem) {
    return item
  }

  @Action({commit: 'loadOSFromData', rawError: true })
  public loadServiceOrderFromData(item: any) {
    return item
  }
}

export default getModule(ServiceOrderModule);
