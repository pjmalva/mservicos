import {
  VuexModule, Module, getModule, Mutation, Action
} from 'vuex-module-decorators'

import { User, UserCredentials, Configuration } from '@/types/modules'
import { makeLogin, clearJWT } from '@/settings/api'
import { AxiosResponse } from 'axios'
import store from '@/store'

@Module({
  store,
  name: 'users',
  dynamic: true,
  namespaced: true,
  preserveState: localStorage.getItem('mg-asd123124ewdfaw43') !== null,
})
class UserModule extends VuexModule{
  user: User | null = null
  configuration: Configuration | null = null

  get isLogged() {
    return localStorage.getItem('@ms-token')
  }

  get currentConfiguration() {
    return this.configuration
  }

  @Mutation setUser(user: User) { this.user = user }
  @Mutation setConfiguration(config: Configuration) { this.configuration = config }

  @Action({commit: 'setUser'})
  async login(UserCredentials: UserCredentials) {
    try {
      const attempt = await makeLogin(UserCredentials)
      return (attempt as AxiosResponse).data.user
    } catch (e) {
      return null
    }
  }

  @Action({commit: 'setUser'})
  logout() {
    clearJWT()
    window.location.href = '/login'
    return null
  }

  @Action({commit: 'setConfiguration'})
  newConfiguration(config: Configuration) {
    return config
  }
}

export default getModule(UserModule);
