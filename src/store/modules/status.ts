import {
  VuexModule, Module, getModule, Mutation, Action
} from 'vuex-module-decorators'
import { ServiceOrderStatus } from '@/types/modules'
import store from '@/store'

@Module({
  store,
  name: 'status',
  dynamic: true,
  namespaced: true,
  preserveState: localStorage.getItem('mg-asd123124ewdfaw43') !== null,
})
class StatusModule extends VuexModule{
  possible: Array<ServiceOrderStatus> = []
  status: ServiceOrderStatus | null = null

  get serviceOrderStatus() {
    return this.status
  }

  @Mutation setStatus(status: ServiceOrderStatus) { this.status = status }

  @Action({commit: 'setStatus'})
  setServiceOrderStatus(status: ServiceOrderStatus) {
    return status
  }
}

export default getModule(StatusModule);
