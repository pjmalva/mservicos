import axios, {
  AxiosError,
  AxiosResponse,
  AxiosRequestConfig
} from 'axios'

import {
  UserCredentials,
  ServiceOrder,
  Business,
  ServiceOrderEquipment,
  ServiceOrderDetails,
  ServiceOrderItem,
  ServiceOrderStatus
} from '@/types/modules'

import order from '../store/modules/os'
import user from '../store/modules/user'
import { param } from 'jQuery'

const token = localStorage.getItem('@ms-token')
export const api = axios.create({
  baseURL: user.currentConfiguration?.api
})

api.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
api.defaults.headers.common['Content-Type'] = 'application/json'

if(token) {
  api.defaults.headers.common['Authorization'] = `Bearer ${ token }`
}

export function setJWT(jwt: string) {
  api.defaults.headers.common['Authorization'] = `Bearer ${ localStorage.getItem('@ms-token') }`
  localStorage.setItem('@ms-token', jwt)
}

export function clearJWT() {
  delete api.defaults.headers.common['Authorization']
  localStorage.removeItem('@ms-token')
}

async function makeRequest (config: AxiosRequestConfig): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.request(config) //.request() get('/order-service')
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function getParameters (): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.get(`/parameter`)
    const data = response.data.data[0]
    order.setParameters({
      serviceOrderVerifiedStatus: data.service_order_verified_status,
      serviceOrderApprovedStatus: data.service_order_approved_status,
      serviceOrderCanceledStatus: data.service_order_canceled_status,
      serviceOrderFinishedStatus: data.service_order_finished_status,
      serviceOrderWarrantyDays: data.service_order_waranty_days,
      serviceOrderText: data.service_order_text,
    })

    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function makeLogin (user: UserCredentials): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.post('/oauth/login', {
      email: user.email,
      password: user.password,
      remember: user.remember
    })

    setJWT(response.data.access_token)

    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function listOs (): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.get('/service-order')
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function getOs (os: BigInteger): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.get(`/service-order/${ os }`)
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function postOs (os: ServiceOrder): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.post('/service-order',{
      os
    })
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function deleteOs (os: BigInteger): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.delete(`/service-order/${ os }`)
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function listService (page=1): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.get(`/service?page=${page}`)
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function listSummary (): Promise<AxiosResponse | AxiosError> {
  try {
    const response = await api.get('/service-order-summary')
    return (response as AxiosResponse)
  } catch (e) {
    return (e as AxiosError)
  }
}

export async function searchEquipment (serieNumber: string): Promise<ServiceOrderEquipment | null> {
  try {
    const response = await api.get(`/equipment/${serieNumber}`)
    const data = (response as AxiosResponse).data
    const equipment = {
      id: data.id,
      serieNumber: data.serie_number,
      name: data.name,
      defects: data.defects,
      image: data.image,
      description: '',
      technicalReport: '',
      accessories: '',
      observations: '',
    }
    return (equipment as ServiceOrderEquipment)
  } catch (e) {
    return null
  }
}


export async function searchCompany (document: string): Promise<Business | null> {
  try {
    const response = await api.get(`/clients?cnpj=${ document }`)
    const data = (response as AxiosResponse).data.data[0]
    // console.log(data)
    const business = {
      id: data.id,
      cnpj: data.cnpj,
      ie: data.ie,
      im: data.im,
      name: data.name,
      fantasyName: data.fantasyName,
      logo: data.logo,
      cep: data.cep,
      address: data.address,
      complement: data.complement,
      number: data.number,
      neighborhood: data.neighborhood,
      city: data.city,
      state: data.state,
      country: data.country,
      phone: data.phone,
      cellphone: data.cellphone,
      email: data.email,
    }
    return ( business as Business )
  } catch (e) {
    if ( document.length == 14 ) {
      try {
        const response = await api.get(`https://cors-anywhere.herokuapp.com/https://www.receitaws.com.br/v1/cnpj/${ document }`)
        const data = (response as AxiosResponse).data
        const business = {
          cnpj: data.cnpj,
          name: data.nome,
          fantasyName: data.fantasia,
          cep: data.cep,
          address: data.logradouro,
          complement: data.complemento,
          number: data.numero,
          neighborhood: data.bairro,
          city: data.municipio,
          state: data.uf,
          phone: data.telefone,
          email: data.email,
        }
        return ( business as Business )
      } catch ( e ) {
        return null
      }
    } else {
      return null
    }
  }
}

export async function postServiceOrder (
  order: ServiceOrder | null,
  client: Business | null,
  details: ServiceOrderDetails | null,
  equipment: ServiceOrderEquipment | null,
  items: Array<ServiceOrderItem>,
  status: ServiceOrderStatus | null
): Promise<AxiosResponse | null>{
  try {
    const response = await api.post(`/service-order`, {
      order,
      client,
      details,
      equipment,
      items,
      status
    })

    return response
  } catch(e) {
    return null
  }
}

export async function listStatus () {
  try {
    const response = await api.get(`/possible-statuses`)
    return response
  } catch(e) {
    return null
  }
}

export async function postStatus (status: any) {
  try {
    const response = await api.post(`/possible-statuses`, { status })
    return response
  } catch(e) {
    return null
  }
}
