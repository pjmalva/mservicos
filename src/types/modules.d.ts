export interface User {
  id: BigInteger,
  name: string,
  email: string,
  password?: string,
  photo?: string
}

export interface UserCredentials {
  email: string,
  password: string,
  remember?: boolean
}

export interface Company {
  id: BigInteger,
  cnpj?: string,
  ie?: string,
  im?: string,
  name: string,
  fantasy_name: string,
  logo: string,
  cep: string,
  address: string,
  complement?: string,
  number?: string,
  neighborhood: string,
  city?: string,
  city_code?: string,
  state?: string,
  state_code?: string,
  country?: string,
  phone?: string,
  cellphone?: string,
  email_contact?: string,
  email_nfe?: string,
}

export interface Business {
  id?: BigInteger,
  cnpj?: string,
  ie?: string,
  im?: string,
  name: string,
  fantasyName: string,
  logo?: string,
  cep: string,
  address?: string,
  complement?: string,
  number?: string,
  neighborhood?: string,
  city?: string,
  state?: string,
  country?: string,
  phone?: string,
  cellphone?: string,
  email?: string,
}

export interface Product {
  id: BigInteger,
  reference: string,
  name: string,
  description?: string,
  barcode?: string,
  image?: string,
  unity?: string,
  isInteger: boolean,
}

export interface Service {
  id: BigInteger,
  reference: string,
  name: string,
  value: string,
  forHour: boolean,
  isActived: boolean,
}

export interface PossibleStatus {
  id?: BigInteger,
  name: string,
  color?: string,
  order?: BigInteger,
  is_actived: boolean
}

export interface ServiceOrderEquipment {
  id?: BigInteger,
  serieNumber: string,
  name?: string,
  description?: string,
  defects?: string,
  image?: string,
  technicalReport?: string,
  accessories?: string,
  observations?: string,
}

export interface ServiceOrder {
  id?: BigInteger,
  number?: string,
  userId: BigInteger,
  statusId?: BigInteger,
  value: number,
  createdAt: Date,
  updatedAt: Date,
  business: Business,
  user: User,
  status?: PossibleStatus
}

export interface ServiceOrderDetails {
  companyId?: BigInteger,
  resultCenterId?: BigInteger,
  stockTypeId?: BigInteger,
  priceTableId?: BigInteger,
  limitWarranty?: Date,
  haveWarranty: boolean,
  wasApproved: boolean,
  wasCanceled: boolean,
  motive?: string,
}

export interface ServiceOrderItem {
  id?: BigInteger,
  serviceOrderId?: BigInteger,
  userId?: BigInteger,
  itemId: BigInteger,
  type: string,
  quantity: number,
  value: number,
  discount: number,
  description?: string,
  wasAproved: boolean,
  serviceOrder?: ServiceOrder,
  user?: User,
  item?: Product | Service,
}

export interface ServiceOrderStatus {
  id?: BigInteger,
  serviceOrderId: BigInteger,
  statusId: BigInteger,
  userId: BigInteger,
  name: string,
  color?: string,
  description?: string,
  start?: Date,
  finish?: Date,
  serviceOrder?: ServiceOrder,
  user: User,
}

export interface Parameters {
  serviceOrderVerifiedStatus?: BigInteger,
  serviceOrderApprovedStatus?: BigInteger,
  serviceOrderCanceledStatus?: BigInteger,
  serviceOrderFinishedStatus?: BigInteger,
  serviceOrderWarrantyDays?: BigInteger,
  serviceOrderText?: string,
}


export interface Configuration {
  api: string,
  storage: string
}
